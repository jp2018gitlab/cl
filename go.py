import asyncio
from pyquery import PyQuery as pq
import requests
import requests_cache
from time import sleep
import datetime as dt
import logging

requests_cache.install_cache()
URLS = """
https://newyork.craigslist.org/search/mnh/hhh?excats=2-16-1-21-1-17-7-34-22&sort=date&bundleDuplicates=1&nh=120&nh=134&nh=160&nh=121&nh=129&nh=122&nh=133&nh=132&nh=127&nh=126&nh=135&nh=136&nh=137&nh=131&nh=125&nh=124&nh=123&nh=130&nh=128&min_price=3000&max_price=4500&min_bedrooms=2&min_bathrooms=2&availabilityMode=1&sale_date=all+dates
https://newyork.craigslist.org/search/mnh/hhh?query=%28nov%7Cnovember%29&excats=2-16-1-21-1-17-7-34-22&sort=date&bundleDuplicates=1&nh=120&nh=134&nh=160&nh=121&nh=129&nh=122&nh=133&nh=132&nh=127&nh=126&nh=135&nh=136&nh=137&nh=131&nh=125&nh=124&nh=123&nh=130&nh=128&min_price=3000&max_price=4500&min_bedrooms=2&min_bathrooms=2&availabilityMode=0&sale_date=all+dates

https://newyork.craigslist.org/search/mnh/hhh?query=%28nov%7Cnovember%29&excats=2-16-1-21-1-17-7-34-22&sort=date&bundleDuplicates=1&nh=120&nh=134&nh=160&nh=121&nh=129&nh=122&nh=133&nh=132&nh=127&nh=126&nh=135&nh=136&nh=137&nh=131&nh=125&nh=124&nh=123&nh=130&nh=128&min_price=4500&max_price=6000&min_bedrooms=3&min_bathrooms=2&availabilityMode=0&sale_date=all+dates
https://newyork.craigslist.org/search/mnh/hhh?excats=2-16-1-21-1-17-7-34-22&sort=date&bundleDuplicates=1&nh=120&nh=134&nh=160&nh=121&nh=129&nh=122&nh=133&nh=132&nh=127&nh=126&nh=135&nh=136&nh=137&nh=131&nh=125&nh=124&nh=123&nh=130&nh=128&min_price=4500&max_price=6000&min_bedrooms=3&min_bathrooms=2&availabilityMode=1&sale_date=all+dates
"""
# 2bdrm
# 3bdrm




def get_list():
	for url in URLS.split('\n'):
		url = url.strip()
		if len(url) == 0:
			continue
		with requests_cache.disabled():
			response = requests.get(url)
		doc = pq(response.text)
		for x in doc('li.result-row a.result-title'):
			x = pq(x)
			yield x.attr('href'), x.text()
		print('****<br/>')

def review(url, html):
	html_l = html.lower()
	if 'Check In-Out Dates' in html:
		return 'spam'
	if 'Check in and checkout date' in html:
		return 'spam'
	if 'harlem' in html_l:
		return 'harlem'
	if 'Let me find your next apartment!' in html:
		return 'agent'
	if 'contact me with your  direct email and phone number' in html:
		return 'spam'
	if 'contact me with your direct email and phone number' in html:
		return 'spam'
	if 'Contact with your Personal Email and Number' in html:
		return 'spam'
	if "..CONTACT WITH YOUR PERSONAL EMAIL AND PHONE NUMBER" in html:
		return 'spam'
	if 'Check In Date & Check Out Date' in html:
		return 'spam'
	if 'Contact for inquiries....' in html:
		return 'spam'
	if "New York, NY 100" in html:
		return 'spam'
	if "Contact With Your Email and Phone No" in html:
		return 'spam'
	if "Contact with your email and Phone no" in html:
		return 'spam'

	if '/mnh/' in url:
		d = pq(html)
		posted = None
		updated = None
		for x in d('p.postinginfo'):
			if 'posted:' in x.text:
				t = pq(pq(x)('time')).attr('datetime')
				posted = dt.datetime.strptime(t, "%Y-%m-%dT%H:%M:%S%z" )
			elif 'updated:' in x.text:
				t = pq(pq(x)('time')).attr('datetime')
				updated = dt.datetime.strptime(t, "%Y-%m-%dT%H:%M:%S%z" )
		if posted is not None and updated is not None and updated - posted > dt.timedelta(days=14):
			return 'updated'

	return True

def get_data(url, html):
	d = pq(html)

	price = 0
	for x in d('span.price'):
		price = pq(x).text()
		break

	return {
		"url": url,
		"price": price,
	}

def main():
	seen = set()

	listings = get_list()

	for url, name in listings:
		token = '/'.join(url.split('/')[:-1])
		if token in seen:
			continue
		seen.add(token)
		response = requests.get(url)
		if review(url, response.text) == True:
			data = get_data(url, response.text)
			print(f'<a href="{url}">{url}</a> - {data["price"]}<br/>')



if __name__ == "__main__":
	main()
